import { createStore, applyMiddleware, compose } from 'redux';
import { logger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';

import myReducer from './reducers';
import rootSagas from './sagas';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  myReducer,
  {},
  compose(applyMiddleware(logger), applyMiddleware(sagaMiddleware)),
);

sagaMiddleware.run(rootSagas);
export default store;