import {Alert, AsyncStorage} from 'react-native';
import {call, put, takeLatest} from 'redux-saga/effects';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

import * as Types from './action';

function* registerSaga(action) {
  try {
    const {email, password} = action.payload;
    const registation = yield auth().createUserWithEmailAndPassword(email, password);
    const user = registation.user;
    firestore()
    .collection('Users')
    .add({
      displayName: user.displayName,
      email: user.email,
      emailVerified: user.emailVerified,
      isAnonymous: user.isAnonymous,
      metadata: user.metadata,
      phoneNumber: user.phoneNumber,
      photoURL: user.photoURL,
      providerData: user.providerData,
      providerId: user.providerId,
      // refreshToken: user.refreshToken,
      // tenantId: user.tenantId,
      uid: user.uid
    });
    yield put(Types.addUserSuccess(action.payload));
  } catch (error) {
    console.log(error);
    yield put(Types.addUserFailure({error}));
  }
}

 function* loginSaga(action) {  
  try {
    const {email, password} = action.payload;
    yield auth().signInWithEmailAndPassword(email, password);
    
    yield put(Types.loginUserSuccess(action.payload));
    AsyncStorage.setItem('token', JSON.stringify(action.payload));  
  } catch (error) {
    Alert.alert(
      "Notification",
      "The password is invalid or the user does not have a password."
    );
    yield put(Types.loginUserFailure({error}));
  }
}

export function* userSagas() {
  yield takeLatest(Types.ADD_USER_REQUEST, registerSaga);
  yield takeLatest(Types.LOGIN_USER_REQUEST, loginSaga);
}