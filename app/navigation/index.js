import React, { useEffect } from 'react';
import { Provider as PaperProvider } from 'react-native-paper';
import { Provider } from 'react-redux';

import { AuthProvider } from './AuthProvider';
import Routes from './Routes';
import firebaseSDK from '../config/firebaseSDK';
import store from '../redux/store';

/**
 * Wrap all providers here
 */

export default function Providers() {
  useEffect(() => {
    firebaseSDK.default;
  }, []);
  return (
    <Provider store={store}>
      <PaperProvider>
        <AuthProvider>
          <Routes />
        </AuthProvider>
      </PaperProvider>
    </Provider>
  );
}