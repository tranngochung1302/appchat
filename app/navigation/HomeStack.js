import React, { useContext } from 'react';
import { TouchableOpacity } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';

import HomeScreen from '../main/Home';
import RoomScreen from '../screens/RoomScreen';
import AddRoomScreen from '../screens/AddRoomScreen';
import { AuthContext } from './AuthProvider';
import Profile from '../main/Profile';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

// create two new instances
const ChatAppStack = createStackNavigator();
const ProfileStack = createStackNavigator();

const shouldTabBarVisible = (route) => {
  const routeName = getFocusedRouteNameFromRoute(route);
  const hideOnScreens = ['Room'];
  if(hideOnScreens.indexOf(routeName) > -1)
    return false;
  return true;
};

function Home() {
  const { logout } = useContext(AuthContext);
  return (
    <ChatAppStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#6646ee',
        },
        headerTintColor: '#ffffff',
        headerTitleStyle: {
          fontSize: 22,
        },
      }}
    >
      <ChatAppStack.Screen
        name='Home'
        component={HomeScreen}
        options={({ navigation }) => ({
          headerLeft: () => (
            <Icon
              name='log-out-outline'
              size={28}
              color='#ffffff'
              // onPress={() => navigation.navigate('Profile')}
              onPress={() => logout()}
            />
          ),
          headerRight: () => (
            <TouchableOpacity onPress={() => navigation.navigate('AddRoom')}>
              <Icon name="add-circle-outline" size={28} color='#ffffff' />
            </TouchableOpacity>
            
          ),
        })}
      />  
      <ChatAppStack.Screen
        name='Room'
        component={RoomScreen}
        options={({ route, navigation }) => ({
          title: route.params.thread.name,
          headerLeft: () => (
            <Icon
              name='arrow-back-outline'
              size={28}
              color='#ffffff'
              onPress={() => navigation.goBack()}
            />
          )
        })}
      />
      <ChatAppStack.Screen
        name='AddRoom'
        component={AddRoomScreen}
      />
    </ChatAppStack.Navigator>
  );
}

function profile() {
  return (
    <ProfileStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#6646ee',
        },
        headerTintColor: '#ffffff',
        headerTitleStyle: {
          fontSize: 22,
        },
      }}
    >
      <ProfileStack.Screen
        name='Profile'
        component={Profile}
        options={({ navigation }) => ({
          title: 'Profile',
          // headerLeft: () => (
          //   <Icon
          //     name='arrow-back-outline'
          //     size={28}
          //     color='#ffffff'
          //     onPress={() => navigation.goBack()}
          //   />
          // )
        })}
      />
    </ProfileStack.Navigator>
  );
}

const Tab = createBottomTabNavigator();

export default function HomeStack() {
  return (
    <Tab.Navigator initialRouteName="Home" headerMode = 'none'>
      <Tab.Screen
        name='Home'
        component={Home}
        options={({ route }) => ({
          tabBarVisible: shouldTabBarVisible(route),
          tabBarIcon: ({ color, size }) => (
            <Icon name="home-outline" size={22} color='#6646ee' />
          ),
        })}
      />
      <Tab.Screen
        name='Profile'
        component={profile}
        options={({ route }) => ({
          tabBarIcon: ({ color, size }) => (
            <Icon name="settings-outline" size={22} color='#6646ee' />
          ),
        })}
      />
    </Tab.Navigator>
  );
}