import React, { useContext, useEffect, useState } from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity, Text } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import { Divider, List, Title } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import { ScrollView } from 'react-native-gesture-handler';

import FormInput from '../components/FormInput';
import FormButton from '../components/FormButton';
import { AuthContext } from '../navigation/AuthProvider';

export default function AddRoomScreen({ navigation }) {
  const [roomName, setRoomName] = useState('');
  const { user, setUser } = useContext(AuthContext);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    firestore()
    .collection('Users')
    .onSnapshot(querySnapshot => {
      const users = querySnapshot.docs.map(documentSnapshot => {
        return {
          _id: documentSnapshot.id,
          // give defaults
          name: '',
          ...documentSnapshot.data()
        };
      });
      setUsers(users);
    });
  });
  // ... Firestore query will come here later
  function handleButtonPress(item) {
    if (roomName.length > 0) {
      const rid = user.uid + item.uid;
      firestore()
      .collection('THREADS')
      .add({
        name: roomName,
        latestMessage: {
          text: `You have joined the room ${roomName}.`,
          createdAt: new Date().getTime()
        },
        rid,
        createdAt: new Date().getTime()
      });
      navigation.navigate('Home');
    }
  }

  return (
    <ScrollView style={styles.rootContainer}>
      <View style={styles.innerContainer}>
        <Title style={styles.title}>Create a new chat room</Title>
        <FormInput
          labelName='Room Name'
          value={roomName}
          onChangeText={(text) => setRoomName(text)}
          clearButtonMode='while-editing'
        />
        <FormButton
          title='Create'
          modeValue='contained'
          labelStyle={styles.buttonLabel}
          onPress={() => handleButtonPress()}
          disabled={roomName.length === 0}
        />
      </View>
      <View style={{alignItems: 'center'}}>
        <FlatList
          data={users}
          keyExtractor={item => item._id}
          ItemSeparatorComponent={() => <Divider />}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() => handleButtonPress(item)}
            >
              <List.Item
                title={item.name || item.email}
                // description={item.latestMessage.text}
                // left={() => <Image
                //   style={styles.tinyLogo}
                //   source={{
                //     uri: 'https://reactnative.dev/img/tiny_logo.png',
                //   }}
                // />}
                left={() => (
                  <View style={styles.listViewLeft}>
                    <Text style={styles.listName}>{(item.name || item.email).charAt(0).toUpperCase()}</Text>
                  </View>
                )}
                right={() => (
                  <Icon name="add-circle-outline" size={28} color='#6646ee' />
                )}
                titleNumberOfLines={1}
                titleStyle={styles.listTitle}
                descriptionStyle={styles.listDescription}
                descriptionNumberOfLines={1}
              />
            </TouchableOpacity>
          )}
        />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
  },
  closeButtonContainer: {
    position: 'absolute',
    top: 30,
    right: 0,
    zIndex: 1,
  },
  innerContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    marginBottom: 10,
  },
  buttonLabel: {
    fontSize: 22,
  },

  listTitle: {
    fontSize: 22
  },
  listViewLeft: {
    alignSelf: 'center',
    padding: 10,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5,
    backgroundColor: 'blue'
  },
  listName: {
    fontSize: 20,
    color: 'white'
  },
  tinyLogo: {
    width: 50,
    height: 50,
    alignSelf: 'center'
  },
  listDescription: {
    fontSize: 16
  }
});