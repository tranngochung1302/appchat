import React, { useState, useContext, useEffect, useRef } from 'react';
import {
  GiftedChat,
  Bubble,
  Send,
  SystemMessage
} from 'react-native-gifted-chat';
import { ActivityIndicator, View, StyleSheet, Text, TouchableOpacity, Image, Alert } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import Clipboard from '@react-native-community/clipboard';
import Icon from 'react-native-vector-icons/Ionicons';
import Tooltip from 'rn-tooltip';
import ScrollBottomSheet from 'react-native-scroll-bottom-sheet';
import Video from 'react-native-video';
import storage from '@react-native-firebase/storage';
import DocumentPicker from 'react-native-document-picker';

import { AuthContext } from '../navigation/AuthProvider';

export default function RoomScreen({ route }) {
  const [messages, setMessages] = useState([]);
  const [users, setUsers] = useState([]);
  const [isTyping, setIsTyping] = useState(false);
  const [customText, setCustomText] = useState('');
  const [transferred, setTransferred] = useState(0);

  const { thread } = route.params;
  const { user } = useContext(AuthContext);
  const currentUser = user.toJSON();

  async function handleSend(messages) {
    const text = messages[0].text;

    firestore()
      .collection('THREADS')
      .doc(thread._id)
      .collection('MESSAGES')
      .add({
        text,
        createdAt: new Date().getTime(),
        user: {
          _id: currentUser.uid,
          email: currentUser.email
        }
      });

    await firestore()
      .collection('THREADS')
      .doc(thread._id)
      .set(
        {
          latestMessage: {
            text,
            createdAt: new Date().getTime()
          }
        },
        { merge: true }
      );
  }

  useEffect(() => {
    firestore()
    .collection('Users')
    .get()
    .then(querySnapshot => {
      const users = querySnapshot.docs.map(documentSnapshot => {
        return {
          _id: documentSnapshot.id,
          // give defaults
          name: '',
          ...documentSnapshot.data()
        };
      });

      setUsers(users);
    });
    // const messagesListener = firestore()
    firestore()
      .collection('THREADS')
      .doc(thread._id)
      .collection('MESSAGES')
      .orderBy('createdAt', 'desc')
      .onSnapshot(querySnapshot => {
      // .then(querySnapshot => {
        const messages = querySnapshot.docs.map(doc => {
          const firebaseData = doc.data();

          const data = {
            _id: doc.id,
            text: '',
            createdAt: new Date().getTime(),
            ...firebaseData
          };

          if (!firebaseData.system) {
            data.user = {
              ...firebaseData.user,
              name: firebaseData.user.email
            };
          }

          return data;
        });
        setMessages(messages);
      });

    // Stop listening for updates whenever the component unmounts
    // return () => messagesListener();
  }, []);

  function renderBubble(props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          right: {
            backgroundColor: '#6646ee'
          },
          left: {
            backgroundColor: '#ffffff'
          }
        }}
        textStyle={{
          right: {
            color: '#ffffff'
          }
        }}
      />
    );
  }

  function renderLoading() {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size='large' color='#6646ee' />
      </View>
    );
  }

  function renderSend(props) {
    return (
      <Send {...props}>
        <View style={styles.sendingContainer}>
          <Icon name="send-outline" size={22} color='#6646ee' />
        </View>
      </Send>
    );
  }

  function scrollToBottomComponent() {
    return (
      <View style={styles.bottomComponentContainer}>
        <Icon name="chevron-down-outline" size={22} color='#6646ee' />
      </View>
    );
  }

  function renderSystemMessage(props) {
    return (
      <SystemMessage
        {...props}
        wrapperStyle={styles.systemMessageWrapper}
        textStyle={styles.systemMessageText}
      />
    );
  }

  async function onDelete(_id, messageIdToDelete) {
    if (_id === currentUser.uid) {
      firestore()
      .collection('THREADS')
      .doc(thread._id)
      .collection('MESSAGES')
      .doc(messageIdToDelete)
      .delete();
    }
    const messageResult = messages.filter(message => message._id !== messageIdToDelete);
    // setMessages(messageResult);
    if (messageResult.length > 0) {
      await firestore()
        .collection('THREADS')
        .doc(thread._id)
        .set(
          {
            latestMessage: {
              text: messageResult[0].text,
              createdAt: messageResult[0].createdAt
            }
          },
          { merge: true }
        );
    } else {
      await firestore()
        .collection('THREADS')
        .doc(thread._id)
        .set(
          {
            latestMessage: {
              text: 'No message',
              createdAt: new Date().getTime()
            }
          },
          { merge: true }
        );
    }
  }

  function onLongPress(context, message) {
    const options = ['copy', 'Cancel'];
    message.user._id === currentUser.uid ? options.splice(1, 0, 'Delete Message') : null;
    const cancelButtonIndex = options.length;
    context.actionSheet().showActionSheetWithOptions({
        options,
        cancelButtonIndex
    }, (buttonIndex) => {
        switch (buttonIndex) {
            case 0:
                Clipboard.setString(message.text);
                break;
            case 1:
                onDelete(message.user._id, message._id) //pass the function here
                break;
        }
    });
  }

  function setTyping(text) {
    setCustomText(text);
    if (!!text && user._id) {
      setIsTyping(true);
    } else {
      setIsTyping(false);
    }
  }

  chooseFile = async () => {
		try {
			const res = await DocumentPicker.pick({
				type: [DocumentPicker.types.allFiles],
			});
			const file = {
				filename: res[0].name,
				size: res[0].size,
				mime: res[0].type,
				uri: res[0].uri,
			};

      const { uri } = file;
      const filename = uri.substring(uri.lastIndexOf('/') + 1);
      const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
      // setUploading(true);
      // setTransferred(0);

      const task = storage()
        .ref(filename)
        .putFile(uploadUri);
    //   storage().ref().putFile(uploadUri).then(function(snapshot) {
    //     storage().ref().getDownloadURL().then(function(url) {
    //         file.public_url = url //merge public url to file object
    //         console.log(url)
    //     }).catch(function(error) {
    //         console.log("error at storageRef.putFile() ", error)
    //     });
    // }).catch(function(e) {
    //     console.log("Upload Error ", e)
    // });
      // set progress state
      // task.on('state_changed', snapshot => {
      //   setTransferred(
      //     Math.round(snapshot.bytesTransferred / snapshot.totalBytes) * 10000
      //   );
      // });
      try {
        await task;
      } catch (e) {
        console.error(e);
      }
      // setUploading(false);
      Alert.alert(
        'Photo uploaded!',
        'Your photo has been uploaded to Firebase Cloud Storage!'
      );

			// if (this.canUploadFile(file)) {
			// 	this.openShareView([file]);
			// }
		} catch (e) {
      console.log(e);
			// if (!DocumentPicker.isCancel(e)) {
			// 	log(e);
			// }
		}
	};

  function micBtn(sendProps) {
    return (
      <View style={styles.sendingContainer}>
        <TouchableOpacity onPress={() => chooseFile()}>
          <Icon name="add-circle-outline" size={30} color='blue' />
        </TouchableOpacity>
      </View>
      // <View flexDirection='row' onTouchStart={messages => micBtnPressed(sendProps.messages)}>
      //   <TouchableOpacity style={styles.sendingContainer} activeOpacity={0.5}>
      //     <Icon name="add-circle-outline" size={30} color='blue' />
      //   </TouchableOpacity>
      // </View>
    );
  }

  // function micBtnPressed(messages=[]) {
  //   //do something useful here
  //   console.log("Current Inputbox content: ",messages)
  // }

  function handleFullScreenToPortrait() {
    Orientation.lockToPortrait();
    this.setState({
      video_url: "",
      paused: true
    });
  };

  function renderMessageVideo(props) {
    const uri = `https://firebasestorage.googleapis.com/v0/b/chatapp-8f5f9.appspot.com/o/${ props.currentMessage.video }?alt=media&token=${ props.currentMessage.tokenURL }`
    // const uri = storage().ref('videoDemo.mp4').getDownloadURL();
    
    return (
      <View style={{ position: 'relative', height: 150, width: 250 }}>
        <Video
          style={{
            position: 'absolute',
            left: 0,
            top: 0,
            height: 150,
            width: 250,
            borderRadius: 20,
          }}
          shouldPlay
          isLooping
          rate={1.0}
          resizeMode="cover"
          height={150}
          width={250}
          muted={false}
          paused={false}
          source={{ uri }}
          onFullscreenPlayerDidDismiss={() => handleFullScreenToPortrait}
          allowsExternalPlayback={false}
        />
      </View>
    )
  }

  function renderMessageImage(props) {
    const uri = `https://firebasestorage.googleapis.com/v0/b/chatapp-8f5f9.appspot.com/o/${ props.currentMessage.image }?alt=media&token=${ props.currentMessage.tokenURL }`
    return (
      <View style={{ position: 'relative', height: 100, width: 150 }}>
        <Image
          style={{
            position: 'absolute',
            left: 0,
            top: 0,
            borderRadius: 20,
          }}
          resizeMode="cover"
          height={100}
          width={150}
          source={{ uri }}
        />
      </View>
    )
  }

  function renderFooter() {
    if (isTyping){
      return (<Text>{user.displayName} is typing</Text>)
    }
    return null;
  }

  return (
    <GiftedChat
      messages={messages}
      onSend={handleSend}
      user={{ _id: currentUser.uid }}
      placeholder='Type your message here...'
      alwaysShowSend
      // showUserAvatar
      scrollToBottom
      renderFooter={renderFooter}
      // renderAvatar={() => <Icon name="send-outline" size={22} color='#6646ee' />}
      renderBubble={renderBubble}
      renderLoading={renderLoading}
      renderMessageImage={renderMessageImage}
      renderMessageVideo={renderMessageVideo}
      renderSend={renderSend}
      scrollToBottomComponent={scrollToBottomComponent}
      renderSystemMessage={renderSystemMessage}
      onLongPress={onLongPress}
      renderActions={messages => micBtn(messages)}
      text={customText}
      onInputTextChanged={text => setTyping(text)}
      isTyping={true}
    />
  );
}

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  sendingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    //backgroundColor: '#485a96',
    borderWidth: 0.5,
    borderColor: '#fff',
    height: 40,
    width: 35,
    borderRadius: 5,
    margin: 5,
  },
  bottomComponentContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  systemMessageWrapper: {
    backgroundColor: '#6646ee',
    borderRadius: 4,
    padding: 5
  },
  systemMessageText: {
    fontSize: 14,
    color: '#ffffff',
    fontWeight: 'bold'
  },
  MicIconStyle: {
    padding: 5,
    marginRight: 10,
    height: 35,
    width: 35,
    resizeMode: 'contain',
  },
  container: {
    flex: 1,
  },
  contentContainerStyle: {
    padding: 16,
    backgroundColor: '#F3F4F9',
  },
  header: {
    alignItems: 'center',
    backgroundColor: 'white',
    paddingVertical: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20
  },
  panelHandle: {
    width: 40,
    height: 2,
    backgroundColor: 'rgba(0,0,0,0.3)',
    borderRadius: 4
  },
  item: {
    padding: 20,
    justifyContent: 'center',
    backgroundColor: 'white',
    alignItems: 'center',
    marginVertical: 10,
  }
});