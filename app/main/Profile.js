import React, { useState, useContext, useEffect } from 'react';
import { View, StyleSheet, StatusBar } from 'react-native';
import { Title } from 'react-native-paper';
import FormInput from '../components/FormInput';
import FormButton from '../components/FormButton';
import { AuthContext } from '../navigation/AuthProvider';
import firestore from '@react-native-firebase/firestore';


export default function Profile({ navigation }) {
  const { updateProfile, user, setUser } = useContext(AuthContext);
  const [email, setEmail] = useState(user.email);
  const [name, setName] = useState(user.displayName ? user.displayName : '');
  const [phoneNumber, setPhoneNumber] = useState(user.phoneNumber ? user.phoneNumber : 0);

  const onPressUpdateProfie = () => {
    firestore()
    .collection('Users')
    .get()
    .then(querySnapshot => {
      querySnapshot.docs.map(documentSnapshot => {
          if (user.uid === documentSnapshot.data().uid) {
            return updateProfile(documentSnapshot.id, name, phoneNumber);
          }
      });
    });
    setUser({
        ...user, _user: {...user._user, displayName: name, phoneNumber: phoneNumber}
    });
    
  }

  return (
    <View style={styles.container}>
      <StatusBar
        animated={true}
        backgroundColor="#6646ee"
      />
      <Title style={styles.titleText}>Update Account</Title>
      <FormInput
        labelName='Email'
        value={email}
        autoCapitalize='none'
        onChangeText={userEmail => setEmail(userEmail)}
        keyboardType='email-address'
        editable={false}
        selectTextOnFocus={false}
      />
      <FormInput
        labelName='Name'
        value={name}
        onChangeText={userName => setName(userName)}
      />
      <FormInput
        labelName='Phone Number'
        value={phoneNumber.toString()}
        onChangeText={userPhoneNumber => setPhoneNumber(userPhoneNumber)}
        keyboardType="numeric"
      />
      <FormButton
        title='Update'
        modeValue='contained'
        labelStyle={styles.loginButtonLabel}
        disabled={(email.length === 0 || name.length === 0 || phoneNumber.length === 0)}
        onPress={onPressUpdateProfie}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f5f5',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleText: {
    fontSize: 24,
    marginBottom: 10
  },
  loginButtonLabel: {
    fontSize: 22
  },
  navButtonText: {
    fontSize: 16
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
  },
});