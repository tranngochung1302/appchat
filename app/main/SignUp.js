import React, { useState, useContext } from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Title } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';

import FormInput from '../components/FormInput';
import FormButton from '../components/FormButton';
import { addUserRequest } from '../redux/actions/userAction/action';
import Loading from '../components/Loading';

function SignupScreen(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const hangdleSignUp = async () => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    if (reg.test(email) === false) {
      alert("Email is not correct");
      return false;
    }
    if (password.length > 5) {
      await props.addUserHangdle({email, password});
    } else {
      alert('Password is not less than 8 characters')
    }
  }

  if (props.user.loading) {
    return <Loading />;
  }

  return (
    <View style={styles.container}>
      <View style={styles.closeButtonContainer}>
        <Icon
          name="close-circle-outline"
          size={36}
          color='#6646ee'
          onPress={() => props.navigation.goBack()}
        />
      </View>
      <View style={styles.content}>
        <Title style={styles.titleText}>Register to chat</Title>
        <FormInput
          labelName='Email'
          value={email}
          autoCapitalize='none'
          onChangeText={userEmail => setEmail(userEmail)}
        />
        <FormInput
          labelName='Password'
          value={password}
          secureTextEntry={true}
          onChangeText={userPassword => setPassword(userPassword)}
        />
        <FormButton
          title='Signup'
          modeValue='contained'
          labelStyle={styles.loginButtonLabel}
          disabled={(email.length === 0 || password.length === 0)}
          onPress={hangdleSignUp}
        />
      </View>
    </View>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  addUserHangdle: async (data) => dispatch(addUserRequest(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(SignupScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  content: {
    backgroundColor: '#f5f5f5',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  closeButtonContainer: {
    position: 'absolute',
    top: 30,
    right: 0,
    zIndex: 1,
  },
  titleText: {
    fontSize: 24,
    marginBottom: 10
  },
  loginButtonLabel: {
    fontSize: 22
  }
});