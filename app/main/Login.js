import React, { useState, useContext } from 'react';
import { View, StyleSheet, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import { Title } from 'react-native-paper';

import FormInput from '../components/FormInput';
import FormButton from '../components/FormButton';
import { AuthContext } from '../navigation/AuthProvider';
import Loading from '../components/Loading';
import { loginUserRequest } from '../redux/actions/userAction/action';


function LoginScreen(props) {
  const { login } = useContext(AuthContext);
  const [email, setEmail] = useState('test@gmail.com');
  const [password, setPassword] = useState('123456');
  const [loading, setLoading] = useState(false);

  const onPressLogin = async () => {
    setLoading(true);
    await props.loginUserHangdle({email, password});
    // login(email, password);
    setLoading(false);
  }

  if (props.user.loading) {
    return <Loading />;
  }

  return (
    <View style={styles.container}>
      <StatusBar
        animated={true}
        backgroundColor="#6646ee"
      />
      <Title style={styles.titleText}>Welcome to Chat app</Title>
      <FormInput
        labelName='Email'
        value={email}
        autoCapitalize='none'
        onChangeText={userEmail => setEmail(userEmail)}
      />
      <FormInput
        labelName='Password'
        value={password}
        secureTextEntry={true}
        onChangeText={userPassword => setPassword(userPassword)}
      />
      <FormButton
        title='Login'
        modeValue='contained'
        labelStyle={styles.loginButtonLabel}
        disabled={(email.length === 0 || password.length === 0)}
        onPress={() => onPressLogin()}
      />
      <FormButton
        title='New user? Join here'
        modeValue='text'
        uppercase={false}
        labelStyle={styles.navButtonText}
        onPress={() => props.navigation.navigate('Signup')}
      />
    </View>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,  
});

const mapDispatchToProps = (dispatch) => ({
  loginUserHangdle: async (data) => dispatch(loginUserRequest(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f5f5',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleText: {
    fontSize: 24,
    marginBottom: 10
  },
  loginButtonLabel: {
    fontSize: 22
  },
  navButtonText: {
    fontSize: 16
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
  },
});