import firebase from '@react-native-firebase/app';

class FirebaseSDK {
  constructor() {
    const firebaseConfig = {
      appId: '1:423792015691:android:19be93773810b577f990ce',
      apiKey: 'AIzaSyAZtC3qMnWKPVjVrmKNWdWAKE05jb2IOfg',
      authDomain: 'chatapp-8f5f9-default-rtdb.firebaseapp.com',
      databaseURL: "https://chatapp-8f5f9-default-rtdb.firebaseio.com",
      projectId: 'chatapp-8f5f9',
      storageBucket: 'chatapp-8f5f9.appspot.com',
      messagingSenderId: '510311633945'
    };
    firebase.apps.length === 0 ? firebase.initializeApp(firebaseConfig) : firebase.app();
  }
}
const firebaseSDK = new FirebaseSDK();
export default firebaseSDK;